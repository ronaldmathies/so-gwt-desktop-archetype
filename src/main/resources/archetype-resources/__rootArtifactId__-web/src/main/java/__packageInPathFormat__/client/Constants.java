#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client;

/**
 * @author Ronald Mathies
 */
public class Constants {

    public final static String MODULE_ID = "${applicationName.toLowerCase()}";
}
