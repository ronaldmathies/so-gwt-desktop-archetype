#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.server.listener;

import ${package}.client.Constants;
import nl.sodeso.gwt.ui.server.listener.AbstractPropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class ${applicationName}PropertiesInitializerContextListener extends AbstractPropertiesInitializerContextListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getServerAppPropertiesClass() {
        return ${applicationName}ServerAppProperties.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getConfiguration() {
        return "${rootArtifactId}-configuration.properties";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }
}
