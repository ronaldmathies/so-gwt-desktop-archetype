#set($symbol_pound='#')
#set($symbol_dollar='$')
#set($symbol_escape='\' )
package ${package}.server.endpoint.session;

import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;
import nl.sodeso.gwt.ui.client.controllers.session.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.security.Principal;

/**
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.session"})
public class SessionEndpoint extends nl.sodeso.gwt.ui.server.endpoint.session.SessionEndpoint {

    /**
     * {@inheritDoc}
     */
    @Override
    public LoginResult login(String username, String password) {
        try {
            getThreadLocalRequest().login(username, password);
            return new LoginResult(new User(username));
        } catch (ServletException e) {}

        return new LoginResult(null);
    }
    
    
    /**
     * {@inheritDoc}
     */
    public LoginResult isLoggedIn() {
        Principal principal = getThreadLocalRequest().getUserPrincipal();
        if (principal != null) {
            return new LoginResult(new User(principal.getName()));
        }

        return new LoginResult(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void logout() {
        try {
            getThreadLocalRequest().logout();
        } catch (ServletException e) {}

        return null;
    }
}
