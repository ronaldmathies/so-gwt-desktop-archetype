#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client;

import com.google.gwt.core.client.GWT;

import nl.sodeso.gwt.ui.client.application.module.ModuleEntryPoint;
import nl.sodeso.gwt.ui.client.application.AvailableModulesService;
import nl.sodeso.gwt.ui.client.application.GwtApplicationEntryPoint;

import ${package}.client.properties.${applicationName}ClientAppProperties;

import java.util.List;
import java.util.logging.Logger;

/**
 * The application entry point is the main method of a GWT application, it should handle
 * the boodstrapping of the application.
 *
 * @author Ronald Mathies
 */
public class ${applicationName}EntryPoint extends GwtApplicationEntryPoint<${applicationName}ClientAppProperties> {

    private static Logger logger = Logger.getLogger("Application.Boot");

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModuleId() {
        return Constants.MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBeforeApplicationLoad() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ModuleEntryPoint> modules(){
        return ((AvailableModulesService)GWT.create(AvailableModulesService.class)).availableModules();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAfterApplicationLoad() {
        // Activate the initial module in this phase.
        // activateModule("[moduleId]");
    }
}
