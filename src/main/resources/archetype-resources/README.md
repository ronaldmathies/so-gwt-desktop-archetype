WHEN USING HIBERNATE
====================

Enable the two properties in the ${rootArtifactId} root pom to be able to use the H2 database.

Please enable the dependecies in the maven-jetty-plugin in the ${rootArtifactId}-web module pom file.

Enable the JNDI resource in the jetty-env.xml file to enable the datasource resource when running without SSL.

Enable the JNDI resource in the jetty.xml file to enable the datasrouce resource when running with SSL.