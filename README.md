Archetype for creating a GWT desktop environment in which you can create modules using the so-gwt-module-archetype.

GETTING STARTED
===============

1. Create a clone of the reposity.
2. Build the archetype project locally using "clean install"
3. Create a new project and choose the "Create from Archetype" option.
4. Click on the "Add Archetype..." button and enter the following information:

    GroupId: nl.sodeso.gwt
    ArtifactId: so-gwt-desktop-archetype
    Version: 1.0-SNAPSHOT

5. Now choose the newly created ArcheType from the list and click 'Next'.
6. Enter your GroupId, ArtifactId en Version of the project you want to create, click 'Next'.
7. Now choose your Maven installation.
8. Now add the following 'extra' variables:

    'applicationName': The name of the application, enter the name in a Class style format, so no spaces (for example: MyApp).
    'applicationTitle': The working name of the application (for example: My Application).
    'baseModuleName': The name of the GWT modules, use only a-z, A-Z.
    'useHibernate': true if the application will use hibernate as it's persistence storage.

9. Click generate.

AFTER GENERATION
================

Check the readme.md of the generated application to see further instructions.

KNOWN ISSUES
===============

1. Sometimes IntelliJ doesn't recognize the modules, just close the project and re-open it, choose the option to delete the project en re-import it.
2. Sometimes the source and resource folders are not recognized, right click on them and choose the corresponding folder type from the 'Mark as...' menu.